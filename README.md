### ¿Qué es este repositorio ###
Este repositorio es el ejercicio final y de presentación en clase del tema 2 de ACDAT.

### A tener en cuenta ###
Modificar los dos EditText del Activity escribiendo la ruta al servidor donde tengas localizado los archivos pertinentes.

Cambiar el valor de la constante **WEB** con la ruta de su servidor hacia el archivo PHP encargado de subir ficheros.