package com.jaime.ficheros;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;

import static java.lang.Thread.sleep;

public class FilesActivity extends AppCompatActivity {
    private static final String FILE_NAME_IMAGES = "enlaces.txt";
    private static final String FILE_NAME_PHRASES = "frases.txt";
    private static final String FILE_NAME_ERRORS = "errores.txt";
    private static final String CODE = "UTF-8";
    private static final int ONE_MINUTE = 60000;
    public final static String WEB = "http://192.168.1.102/acceso/upload.php";


    private EditText mEdtImages;
    private EditText mEdtPhrases;
    private Button mBtnDownload;
    private ImageView mImgvImages;
    private TextView mTxvPhrase;
    private ViewGroup mLayout;

    private List<String> mImagesList;
    private List<String> mPhrasesList;
    private int mImageIndex;
    private int mPhrasesIndex;
    private int mTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_files);

        initialize();
    }

    private void initialize() {
        mImagesList = new ArrayList<>();
        mPhrasesList = new ArrayList<>();
        mImageIndex = 0;
        mPhrasesIndex = 0;
        mTime = 0;

        mEdtImages = (EditText) findViewById(R.id.edt_images);
        mEdtPhrases = (EditText) findViewById(R.id.edt_phrases);
        mImgvImages = (ImageView) findViewById(R.id.imgv_images);
        mTxvPhrase = (TextView) findViewById(R.id.txv_phrase);
        mLayout = (RelativeLayout) findViewById(R.id.activity_files);

        mBtnDownload = (Button) findViewById(R.id.btn_download);
        mBtnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                download(mEdtImages.getText().toString(), FILE_NAME_IMAGES);
                download(mEdtPhrases.getText().toString(), FILE_NAME_PHRASES);
                readFile(FILE_NAME_IMAGES);
                readFile(FILE_NAME_PHRASES);
                getRaw();

                if (!mImagesList.isEmpty() && !mPhrasesList.isEmpty()) {
                    CountDownTimer timer = new CountDownTimer(ONE_MINUTE, mTime) {
                        @Override
                        public void onTick(long l) {
                            showImagesPhrases();
                        }

                        @Override
                        public void onFinish() {

                        }
                    }.start();
                }
            }
        });
    }

    private void download(final String url, String fileName) {
        final ProgressDialog progress = new ProgressDialog(this);
        File myFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fileName);

        RestClient.get(url, new FileAsyncHttpResponseHandler(myFile) {
            @Override
            public void onStart() {
                super.onStart();
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setMessage("Descargando . . .");
                progress.setCancelable(false);
                progress.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progress.dismiss();
                writeError(url, "Error al descargar el fichero");
                uploadErrors();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progress.dismiss();
                Snackbar.make(mLayout, "Descarga finalizada\n " + file.getPath(), Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void readFile(String fileName) {
        File file = null;
        FileInputStream fileInputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader in = null;
        String line = "";

        try {
            file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fileName);
            fileInputStream = new FileInputStream(file);
            inputStreamReader = new InputStreamReader(fileInputStream, CODE);
            in = new BufferedReader(inputStreamReader);
            line = in.readLine();

            while (line != null) {
                if (fileName == FILE_NAME_IMAGES)
                    mImagesList.add(line);
                else if (fileName == FILE_NAME_PHRASES)
                    mPhrasesList.add(line);

                line = in.readLine();
            }

        } catch (FileNotFoundException e) {
            writeError(fileName, "No se encuentra el fichero");
            uploadErrors();
        } catch (UnsupportedEncodingException e) {
            writeError(fileName, "Encoding no soportado");
            uploadErrors();
        } catch (IOException e) {
            writeError(fileName, "No se ha podido leer el fichero");
            uploadErrors();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                writeError(fileName, "Error al cerrar el fichero");
                uploadErrors();
            }
        }
    }

    private void getRaw(){
        InputStream is = null;
        StringBuilder content = new StringBuilder();
        int n;

        try {
            is = getResources().openRawResource(R.raw.intervalo);

            while ((n = is.read()) != -1) {
                content.append((char) n);
            }
        } catch (Exception e) {
            writeError("/res/raw/intervalo.txt", "Error: " + e.getMessage());
            uploadErrors();
        } finally {
            try {
                if (is != null) {
                    is.close();
                    mTime = Integer.parseInt(content.toString());
                }
            } catch (Exception e) {
                writeError("/res/raw/intervalo.txt", "Error al cerrar el fichero RAW");
                uploadErrors();
            }
        }
    }

    private void showImagesPhrases () {
        String url = "";

        url = mImagesList.get(mImageIndex);
        Picasso.with(getApplicationContext())
                .load(url)
                .into(mImgvImages);

        mTxvPhrase.setText(mPhrasesList.get(mPhrasesIndex));

        mImageIndex++;
        mPhrasesIndex++;

        if (mImageIndex >= mImagesList.size())
            mImageIndex = 0;

        if (mPhrasesIndex >= mPhrasesList.size())
            mPhrasesIndex = 0;
    }

    private void writeError (String filePath, String errorMessage) {
        File file = null;
        FileOutputStream fileOutputStream = null;
        OutputStreamWriter outputStreamWriter = null;
        BufferedWriter out = null;
        String errorInfo = "Path: " + filePath + "; Date: " + new Date().toString() + "; Message: " + errorMessage;

        try {
            file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), FILE_NAME_ERRORS);
            fileOutputStream = new FileOutputStream(file, true);
            outputStreamWriter = new OutputStreamWriter(fileOutputStream, CODE);
            out = new BufferedWriter(outputStreamWriter);
            out.write(errorInfo + "\n");

        } catch (FileNotFoundException e) {
            Snackbar.make(mLayout, "ERROR: No se ha encontrado el fichero de errores", Snackbar.LENGTH_SHORT).show();
        } catch (UnsupportedEncodingException e) {
            Snackbar.make(mLayout, "ERROR: " + e.getMessage(), Snackbar.LENGTH_SHORT).show();
        } catch (IOException e) {
            Snackbar.make(mLayout, "ERROR: No se ha podido escribir en el fichero de errores", Snackbar.LENGTH_SHORT).show();
        } finally {
            if (file != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    Snackbar.make(mLayout, "ERROR: Al cerrar el fichero de errores", Snackbar.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void uploadErrors() {
        File file;
        Boolean exist = true;

        file = new File(Environment.getExternalStorageDirectory(), FILE_NAME_ERRORS);
        RequestParams params = new RequestParams();

        try {
            params.put("fileToUpload", file);
        } catch (FileNotFoundException e) {
            exist = false;
            Snackbar.make(mLayout, "Error en el fichero de errores: " + e.getMessage(), Snackbar.LENGTH_SHORT).show();
        }

        if (exist)
            RestClient.post(WEB, params, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Snackbar.make(mLayout, "Fallo: " + statusCode + "\n" + responseString + "\n" + throwable.getMessage(), Snackbar.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {

                }

                @Override
                public void onStart() {

                }
            });
    }
}
